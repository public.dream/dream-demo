import {
  pipe, compose, composeRight,
  recurry, prop, lets, each, id,
  ifOk, ifPredicate, ifTrue, side1, side2,
  path, map, noop, whenOk, invoke, eq,
  join, die, concatTo, not, ok, nil,
  condS, guard, otherwise,
} from 'stick-js/es'

import React, { Fragment, useCallback, useEffect, useRef, useState, } from 'react'

import { connect, useDispatch, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'
import styled from 'styled-components'

import { Nothing, Just, fold, cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { inputValue, keyPressListen, clss, } from 'alleycat-js/es/dom'
import { foldWhenRequestResults, requestIsResults, } from 'alleycat-js/es/fetch'
import { mapX, iwarn, ierror, logWith, setTimeoutOn, trim, pluckN, setIntervalOn, } from 'alleycat-js/es/general'
import { ifTrueV, whenNotEmptyString, any, ifEmptyList, whenNotEmptyList, } from 'alleycat-js/es/predicate'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { ierrorError, errorError, } from 'alleycat-js/es/react-s-alert'
import { useReduxReducer, useSaga, } from 'alleycat-js/es/redux-hooks'
import { media, mediaQuery, } from 'alleycat-js/es/styled'

import {
  dmcForgetMockedLocalFetch,
  dmcGetPdfFetch, /* dmcRepeatSearch */
  dmcSearchFetch, dmcSynchronizeFetch,
} from '../App/actions'
import {} from '../../slices/app/selectors'
import {
  selectDmcIndexKeys,
  selectDmcSearchQuery,
  selectDmcSearchResults,
  selectDmcSynchronize,
} from '../../slices/domain/selectors'

import saga from './saga'

import { spinner, } from '../../alleycat-components'
import { Button, H1, Input, } from '../../components/shared'
import Main from '../../components/Main'

import { container, useWhy, mediaPhone, mediaTablet, mediaDesktop, mediaTabletWidth, requestResults, lookupOnOr, } from '../../common'
import config from '../../config'

import {} from '../../types'

const configTop = config | configure.init

const mapComponent = recurry (2) ((C) => mapX ((props, idx) => <C key={idx} {...props}/>))
const containsOnlyEmptyLists = (o) => {
  for (const i in o) if (o [i].length) return false
  return true
}
const ifContainsOnlyEmptyLists = ifPredicate (containsOnlyEmptyLists)

const icons = configTop.focus ('images.icons')
const imgDownload = icons.get ('download')
const imgView = icons.get ('view')

const AboutS = styled.div``

const About = () => <AboutS>
  <p>This is a demonstration of the DREAM software as of February 21st, 2022.</p>
  <p>It allows you to retrieve a subset of SciHub and make a DMC node from it.</p>
  <p>Then you will be able to search through your local repository and retrieve PDF files from a remote repository.</p>
  <p>Last but not least will be to look through the local network for missing data and add further documents to your DMC repository.</p>
</AboutS>

const SpinnerComet = spinner ('comet')

const Scenario1 = styled.div`
  .link {
    opacity: 1 !important;
    text-decoration: underline;
    cursor: pointer;
    pointer-events: auto;
  }
  .analyse {
    font-size: 12px;
    opacity: 0.8;
    line-height: 1.6em;
  }
`

const ControlsSyncS = styled.div`
  > * {
    margin-right: 10px;
  }
  button {
    vertical-align: top;
  }
  .x__spinner {
    margin-top: 2px;
    margin-left: 15px;
  }
`

const ControlsSync = ({ state, onClick, }) => lets (
  () => state === 'pending',
  () => state === 'done',
  (pending, done) => <ControlsSyncS>
    <Button disabled={pending || done} onClick={onClick}>
      synchronize metadata with main SciHub DMC repo
    </Button>
    <span className='x__spinner'>
      <SpinnerComet spinning={pending} size={12}/>
    </span>
  </ControlsSyncS>,
)

const reactClone = recurry (2) (
  // --- in most cases config is just props (see docs).
  (config) => (kid) => React.cloneElement (kid, config),
)

const kidsMap = recurry (2) (
  (f) => (children) => {
    const g = (kid, ..._) => f (kid)
    return React.Children.map (children, g)
  }
)

// --- usage: children | kidsAddProps (props)
const kidsAddProps = kidsMap << reactClone

const InactiveS = styled.div`
  ${prop ('inactive') >> ifTrueV (
    'opacity: 0.7; pointer-events: none;',
    'opacity: 1',
  )}
`

const Inactive = ({ inactive, children, }) => {
  const ref = useRef ()
  useEffect (() => {
    ; ['button', 'input'] | map (
      (name) => ref.current.querySelectorAll (name) | each (
        ifPredicate (() => inactive === true) (
          (el) => el.setAttribute ('disabled', true),
          (el) => el.removeAttribute ('disabled'),
        ),
      ),
    )
  })
  return <InactiveS ref={ref} inactive={inactive}>
    {children}
  </InactiveS>
}

const Hide = ({ hide, children, }) => hide | ifTrue (
  () => null,
  () => children,
)

const SearchS = styled.div`
  margin-top: 15px;
  .x__ctls {
    height: 30px;
    input {
      height: 100%;
    }
  }
  .x__button {
    margin-left: 10px;
    vertical-align: middle;
  }
`

const Search = ({ search, }) => {
  const [query, setQuery] = useState ('')
  // --- ugly to store this in state, but it's because of the effect run by `Inactive`.
  const [disabled, setDisabled] = useState ()
  const onChange = useCallbackConst (inputValue >> trim >> setQuery)
  const submit = useCallback (() => query | whenNotEmptyString (search), [query])
  const onClick = useCallback (() => submit ())
  const onKeyPress = useCallback (keyPressListen (submit, 'Enter'))
  useEffect (() => {
    setDisabled (query === '')
  })
  return <SearchS>
    <div>
      Search SciHub
    </div>
    <div className='x__ctls'>
      <Input onChange={onChange} onKeyPress={onKeyPress} width='400px'/>
      <span className='x__button'>
        <Button disabled={disabled} style={{height: '100%'}} onClick={onClick}>search</Button>
      </span>
    </div>
  </SearchS>
}

const IndexKeysS = styled.div`
  margin-top: 15px;
`

const IndexKeys = ({ keys, }) => <IndexKeysS>
  <div className='analyse'>
    {keys | foldWhenRequestResults (join (' | ') >> concatTo ('Valid queries: '))}
  </div>
</IndexKeysS>

const SearchResultsS = styled.div`
  margin-top: 20px;
  width: 80%;
  font-size: 80%;
  // white-space: nowrap;
  .x__wrap {
    cursor: pointer;
    margin-top: 10px;
    :hover {
      background: bisque;
    }
    :nth-child(1) {
      margin-top: 0px;
    }
  }
  .x__icon, .x__info {
    display: inline-block;
    vertical-align: top;
  }
  span {
    margin-right: 5px;
  }
  .x__icon {
  }
  .x__info {
    margin-left: 15px;
    white-space: normal;
    width: 90%;
    .x__author {
    }
    .x__title {
      text-decoration: underline;
    }
    .x__date {}
  }
  .x__analyse {
    margin-top: 10px;
  }
`

const IconLocal = ({ width='21px', }) => <img src={imgView} width={width}/>
const IconLocalArea = ({ width='21px', }) => <img style={{ filter: 'hue-rotate(90deg)', color: 'blue', }} src={imgDownload} width={width}/>
const IconWideArea = ({ width='21px', }) => <img style={{ color: 'red', }} src={imgDownload} width={width}/>

const SearchResult = (fetch) => ({ pdfInfo: { availability, erisURN, }, info: { author, title, date, }, }) => {
  const onClickPDF = useCallback (() => fetch (erisURN), [erisURN])

  return <div className='x__wrap' onClick={onClickPDF}>
    <div className='x__icon'>
      {availability | lookupOnOr (
        () => die ('bad API data'),
        {
          local: <IconLocal/>,
          'local-area': <IconLocalArea/>,
          'wide-area': <IconWideArea/>,
        }
      )}
    </div>
    <div className='x__info'>
      <span className='x__author'>
        {author | join (', ')}:
      </span>
      <span className='x__title'>
        {title}
      </span>
      <span className='x__date'>
        ({date})
      </span>
      <div className='x__analyse analyse'>
        {erisURN}
      </div>
    </div>
  </div>
}

const PermissionsS = styled.div`
  pointer-events: none;
  display: inline-block;
  margin-left: 10px;
  font-size: 90%;
  span {
    opacity: 0.6;
  }
`

const Permissions = ({ granted, onGrant, onDeny, }) => {
  const cls = (b) => clss (
    b && 'link',
  )
  return <PermissionsS>
    <span onClick={onGrant} className={cls (granted | not)}>grant permission</span>
    |&nbsp;
    <span onClick={onDeny} className={cls (granted)}>deny permission</span>
  </PermissionsS>
}

const SearchResultsInnerS = styled.div`
  > * {
    margin-top: 15px;
    :nth-child(1) {
      margin-top: 0px;
    }
  }
`

const SearchResultsInner = ({ fetch, local, la, wa, }) => {
  const format = map (pluckN (['pdfInfo', 'info'])) >> mapComponent (SearchResult (fetch))
  const [grantedLocal, setGrantedLocal] = useState ()
  const onGrantLocal = useCallbackConst (() => setGrantedLocal (true))
  const onDenyLocal = useCallbackConst (() => setGrantedLocal (false))
  const [grantedWide, setGrantedWide] = useState ()
  const onGrantWide = useCallbackConst (() => setGrantedWide (true))
  const onDenyWide = useCallbackConst (() => setGrantedWide (false))
  const doLocal = (data) => <>
    <div>locally available</div>
    {data | format}
  </>
  const doNetwork = (text, granted, onGrant, onDeny) => (data) => <>
    <div>
      available on {text}
      <Permissions granted={granted} onGrant={onGrant} onDeny={onDeny}/>
    </div>
    <Inactive inactive={granted | not}>
      {data | format}
    </Inactive>
  </>
  return <SearchResultsInnerS>
    {local | whenNotEmptyList (doLocal)}
    {la | whenNotEmptyList (doNetwork ('local area network', grantedLocal, onGrantLocal, onDenyLocal))}
    {wa | whenNotEmptyList (doNetwork ('wide area network', grantedWide, onGrantWide, onDenyWide))}
  </SearchResultsInnerS>
}

const SearchResults = ({ results, fetch, }) => <SearchResultsS>
  {results | requestResults ({
    onResults: ifContainsOnlyEmptyLists (
      () => 'no results',
      ({ local, 'local-area': la, 'wide-area': wa, }) =>
        <SearchResultsInner fetch={fetch} local={local} la={la} wa={wa}/>,
    ),
  })}
</SearchResultsS>

/*
  const LegendS = styled.div`
    border: 1px solid #999999;
    margin-top: 25px;
    font-size: 12px;
    display: inline-block;
    padding: 8px;
`

const Legend = () => <LegendS>
  <div><IconLocal/> = PDF can be retrieved from our own DMC</div>
  <div><IconLocalArea/> = PDF can be retrieved from a DMC on the local area network</div>
  <div><IconWideArea/> = PDF can be retrieved from a DMC on the wide area network</div>
  </LegendS>
  */

const Forget = ({ forget, }) => {
  const onClick = useCallbackConst (() => forget ())
  return <div className='analyse'>
    <p>After a PDF is downloaded from the network, it will move to the local section as a result of a (mocked) DMC sync operation.
      <br/><span className='link' onClick={onClick}>Click here</span> to reset this mocked state.
    </p>
  </div>
}

const dispatchTable = {
  dmcForgetMockedLocalFetchDispatch: dmcForgetMockedLocalFetch,
  dmcGetPdfFetchDispatch: dmcGetPdfFetch,
  dmcSearchFetchDispatch: dmcSearchFetch,
  dmcSynchronizeFetchDispatch: dmcSynchronizeFetch,
}

const selectorTable = {
  dmcIndexKeys: selectDmcIndexKeys,
  dmcSearchQuery: selectDmcSearchQuery,
  dmcSearchResults: selectDmcSearchResults,
  dmcSynchronize: selectDmcSynchronize,
}

export default container (
  ['Scenario1', dispatchTable, selectorTable],
  (props) => {
    const {
      isMobile,
      dmcIndexKeys,
      dmcSearchQuery, dmcSearchResults, dmcSynchronize,
      dmcForgetMockedLocalFetchDispatch,
      dmcGetPdfFetchDispatch,
      dmcSearchFetchDispatch,
      dmcSynchronizeFetchDispatch,
    } = props

    useSaga ({ saga, key: 'Scenario1', })

    const onClickSync = useCallbackConst (() => dmcSynchronizeFetchDispatch ())

    const [dmcSynchronizePending, dmcSynchronized] = dmcSynchronize | cata ({
      RequestInit: () => [false, false],
      RequestLoading: () => [true, false],
      RequestError: () => [false, false],
      RequestResults: () => [false, true],
    })
    const hasResults = dmcSearchResults | requestIsResults
    const noPreviousSearch = dmcSearchQuery | nil

    const controlsState = dmcSynchronizePending ? 'pending' : dmcSynchronized ? 'done' : 'init'

    /*
    const hideLegend = any (
      () => not (hasResults),
      () => dmcSearchResults | foldWhenRequestResults (containsOnlyEmptyLists),
    )
    */

    useWhy ('Scenario1', props, {})

    return <Main title='scenario 1'>
      <Scenario1>
        <About/>
        <ControlsSync state={controlsState} onClick={onClickSync}/>
        <Inactive inactive={dmcSynchronized | not}>
          <Search search={dmcSearchFetchDispatch}/>
          <IndexKeys keys={dmcIndexKeys}/>
          <SearchResults results={dmcSearchResults} fetch={dmcGetPdfFetchDispatch}/>
        </Inactive>
        <Hide hide={noPreviousSearch}>
          <Forget forget={dmcForgetMockedLocalFetchDispatch}/>
        </Hide>
      </Scenario1>
    </Main>
  }
)
