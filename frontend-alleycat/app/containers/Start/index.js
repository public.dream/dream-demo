import {
  pipe, compose, composeRight,
  split, prop, map, join, assoc,
  whenOk, sprintf1,
  eq, not, ifPredicate, whenPredicate,
  ifOk,
  ifTrue,
  prependTo,
  sprintfN,
  bindProp,
  whenTrue,
  concat, divideBy,
  ifFalse,
  always,
  mergeM,
  id,
  tap,
  die, condS, guard,
  flip3,
  reduce,
  lets, noop,
  mapTuples,
  mapValues,
  invoke,
  repeatF,
} from 'stick-js/es'

import React, { Fragment, useCallback, useEffect, useRef, useState, } from 'react'

import { FormattedMessage, } from 'react-intl'
import { connect, useDispatch, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'
import styled from 'styled-components'

import 'react-json-pretty/themes/monikai.css'
import JSONPretty from 'react-json-pretty'

import { Nothing, Just, fold, cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { stopPropagation, } from 'alleycat-js/es/dom'
import { mapX, iwarn, ierror, logWith, setTimeoutOn, } from 'alleycat-js/es/general'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { ierrorError, errorError, } from 'alleycat-js/es/react-s-alert'
import { useReduxReducer, useSaga, } from 'alleycat-js/es/redux-hooks'
import { media, mediaQuery, } from 'alleycat-js/es/styled'

import { dmcFetch, } from '../App/actions'
import {} from '../../slices/app/selectors'
import { selectDmcTest, } from '../../slices/domain/selectors'

import saga from './saga'

import { spinner, } from '../../alleycat-components'
import { Button, } from '../../components/shared'

import { Header, } from '../../components/Header/Loadable'
import Main from '../../components/Main'

import { container, getMessages, useWhy, mediaPhone, mediaTablet, mediaDesktop, mediaTabletWidth, requestResults, } from '../../common'
import config from '../../config'

import {} from '../../types'

const configTop = config | configure.init

const Results = styled.div`
  min-width: 50px;
  min-height: 50px;
  margin-left: 50px;
`

const dispatchTable = {
  dmcFetchDispatch: dmcFetch,
}

const selectorTable = {
  dmcTest: selectDmcTest,
}

const SpinnerComet = spinner ('comet')
const SpinnerS = styled.div`
  margin-left: 20px;
`
const Spinner = (props) => <SpinnerS>
  <SpinnerComet {... props}/>
</SpinnerS>

const StartS = styled.div`
  .x__test-dmc {
    margin-top: 20px;
  }
  .x__scenarios {
  }
`

const prettyPrint = (json) => <JSONPretty data={json}/>

export default container (
  ['Start', dispatchTable, selectorTable],
  (props) => {
    const {
      isMobile, locale,
      dmcTest,
      dmcFetchDispatch,
    } = props

    useSaga ({ saga, key: 'Start', })

    const onClick = useCallbackConst (() => dmcFetchDispatch ())
    const breakpoint = mediaTabletWidth

    useWhy ('Start', props, {})

    return <StartS>
      <Main>
        <div className='x__test-dmc'>
          <Button onClick={onClick}>
            test DMC connection (get object graph)
          </Button>
          <Results>
            {dmcTest | requestResults ({ Spinner, onResults: prettyPrint, onError: noop, })}
          </Results>
        </div>
        <div className='x__scenarios'>
          <div>
            <a href="/scenario1">Scenario 1 — sync &amp; search</a>
          </div>
          <div>
            <a href="/scenario2">Scenario 2 — create a repo from a bibliography</a>
          </div>
          <div>
            <a href="/scenario3">Scenario 3 — read a custom repo using a QR code</a>
          </div>
        </div>
      </Main>
    </StartS>
  }
)
