import {
  pipe, compose, composeRight,
  ifTrue, sprintf1, ifFalse,
  sprintfN,
  whenPredicate,
  die,
  whenTrue,
  mergeM,
  prop, tap,
  lets,
  append,
  dot1,
  noop,
  arg1,
  prependM, take,
  dot,
  head,
  map,
  invoke,
  path, ok, join, flip, reduce, ifOk, appendM,
  id,
  list, compactOk,
  split,
  defaultToV,
  deconstruct,
  whenOk,
  recurry,
  always,
} from 'stick-js/es'

import React, { useCallback, useEffect, useRef, useState, } from 'react'
import styled from 'styled-components'
import { Switch, Route, } from 'react-router-dom'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import FontFaceObserver from 'fontfaceobserver'

import { then, recover, promiseToEither, allP, } from 'alleycat-js/es/async'
import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { fontFace, cssFont, } from 'alleycat-js/es/font'
import { logWith, info, warn, iwarn, setTimeoutOn, } from 'alleycat-js/es/general'
import { whenPredicateResult, whenTrueV, ifEmptyString, } from 'alleycat-js/es/predicate'
import { useMeasureWithCb, } from 'alleycat-js/es/react'
import { useReduxReducer, useSaga, } from 'alleycat-js/es/redux-hooks'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { createReducer, } from '../../redux'
import domainReducer from '../../slices/domain/reducer'
import uiReducer from '../../slices/ui/reducer'

import appReducer from '../../slices/app/reducer'

import { selectError, } from '../../slices/domain/selectors'

import saga from './saga'

import {} from './actions'
import { Alert, } from '../../alleycat-components'

import { ErrorBoundary, } from '../../components/ErrorBoundary'
import { Start, } from '../../containers/Start/Loadable'
import Scenario1 from '../../containers/Scenario1'
import Scenario2 from '../../containers/Scenario2'
import Scenario3 from '../../containers/Scenario3'
import NotFoundPage from '../../containers/NotFoundPage'
import Toast from '../../components/Toast'

import { container, mediaPhone, mediaTablet, mediaDesktop, isMobileWidth, shouldDisableMomentumScroll, useWhy, } from '../../common'
import config from '../../config'

const configTop = config | configure.init

const fontMainFamily = 'font.main.family' | configTop.get

const fontStyles = [
  ['normal', 'normal'],
  ['normal', 'italic'],
  ['bold', 'normal'],
  ['bold', 'italic'],
]

const loadFont = 'load' | dot
const slice = dot1 ('slice')

const startFontObserver = fontFamily => fontStyles | map (
    ([weight, style]) => new FontFaceObserver (fontFamily, {
      weight,
      style,
    }),
  )
  | map (loadFont)
  | allP
  // --- doesn't always die on failure @todo
  | recover ((fontDetails) => die (
    fontFamily | sprintf1 ('timed out waiting for font %s'),
    fontDetails | JSON.stringify,
  ))

const AppWrapper = styled.div`
  ${mediaQuery (
    mediaPhone ('font-size: 14px'),
    mediaTablet ('font-size: 16px'),
    mediaDesktop ('font-size: 18px'),
  )}
  // --- this is mysterious -- it's a hack which *might* help with scrolling/flickering issues on
  // iPhone5 Safari by forcing GPU.
  transform: translate3d(0, 0, 0);
  position: relative;
  overflow-x: hidden;
  overflow-y: hidden;
  margin: 0 auto;
  height: 100%;
  padding: 15px;
`

const dispatchTable = {
}

const selectorTable = {
  error: selectError,
}

export default container (
  ['App', dispatchTable, selectorTable],
  (props) => {
    useWhy ('App', props)

    const { error, history, } = props

    useReduxReducer ({ createReducer, key: 'domain', reducer: domainReducer, })
    useReduxReducer ({ createReducer, key: 'ui', reducer: uiReducer, })
    useReduxReducer ({ createReducer, key: 'app', reducer: appReducer, })

    useSaga ({ saga, key: 'App', })

    const [isMobile, setIsMobile] = useState (void 8)
    const [fontLoaded, setFontLoaded] = useState (false)

    const [width, ref] = useMeasureWithCb (
      (node, check) => window.addEventListener ('resize', check),
      prop ('width'),
    )

    useEffect (() => {
      fontMainFamily
        | startFontObserver
        // --- if the font fails, keep going.
        | recover (console.error)
        | then (() => setFontLoaded (true))
    }, [])

    useEffect (() => {
      width | whenOk (
        isMobileWidth >> setIsMobile,
      )
    })

    // --- note that after mounting, isMobile is still false for an instant, even on mobile.
    const passProps = { history, isMobile, }
    const cls = clss (
      isMobile | whenTrueV ('x--mobile'),
    )

    return error | ifTrue (
      () => <div>
        <div>Sorry, but we’ve encountered a fatal error.</div>
        <div>Please reload the page and start again.</div>
      </div>,

      () => <ErrorBoundary>
        <Toast/>
        <AppWrapper ref={ref} className={cls}>
          {fontLoaded | ifFalse (
            () => '',
            () => <Switch>
              <Route exact path="/" render={(props) => <Start {...passProps} />}/>
              <Route path="/scenario1" render={(props) => <Scenario1 {...passProps} />}/>
              <Route path="/scenario2" render={(props) => <Scenario2 {...passProps} />}/>
              <Route path="/scenario3" render={(props) => <Scenario3 {...passProps} />}/>
              <Route path="" component={NotFoundPage} />
            </Switch>
            )}
            {Alert ()}
        </AppWrapper>,
      </ErrorBoundary>,
    )
  },
)
