import {
  pipe, compose, composeRight,
  add, multiply, join,
  path,
  sprintf1, sprintfN, ok, dot1, timesF, dot, whenOk,
  compact,
  guard, condS,
  exception, raise,
  invoke,
  concatTo,
  lets,
  letS,
  die,
  defaultTo,
  neu1, notOk,
  drop, noop,
  ifPredicate, xMatch, id, reduceObj,
  tap, map,
  applyTo1,
  whenFalse, prop,
  ifOk,
  appendM,
  eq,
  merge, minus, reduce, mergeM,
  recurry,
  not,
  cond, guardV, otherwise, append,
  concat,
  defaultToV,
  split,
  ifAlways, nil,
  plus,
  update,
  remapTuples,
  flip,
  ifNotOk,
} from 'stick-js/es'

import { all, call, put, select, takeEvery, takeLatest, delay, } from 'redux-saga/effects'

import { Left, Right, fold, flatMap, liftA2, liftA2N, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { requestJSON, defaultOpts, resultFoldMap, resultFold, } from 'alleycat-js/es/fetch'
import { ierror, toString, logWith, iwarn, } from 'alleycat-js/es/general'
import {} from 'alleycat-js/es/predicate'
import { errorError, error as alertError, } from 'alleycat-js/es/react-s-alert'
import { EffAction, EffSaga, EffNoEffect, } from 'alleycat-js/es/saga'

import {
  dmcFetch, dmcFetchCompleted,
  dmcForgetMockedLocalFetch, dmcForgetMockedLocalFetchCompleted,
  dmcGetPdfFetch, dmcGetPdfFetchCompleted,
  dmcIndexKeysFetch, dmcIndexKeysFetchCompleted,
  dmcRepeatSearch,
  dmcSearchFetch, dmcSearchFetchCompleted,
  dmcSynchronizeFetch, dmcSynchronizeFetchCompleted,
} from '../App/actions'
import {} from '../../slices/app/selectors'
import { selectDmcSearchQuery, } from '../../slices/domain/selectors'

import { doApiCall, saga, toastError, } from '../../common'

import config from '../../config'
import {} from '../../types'

export function *s_dmcFetch () {
  yield call (doApiCall, {
    url: '/api/dmc-test',
    resultsModify: map (prop ('data')),
    oops: toastError,
    continuation: EffAction (dmcFetchCompleted),
    imsgDecorate: 'Error calling DMC test',
  })
}

export function *s_dmcForgetMockedLocalFetch () {
  yield call (doApiCall, {
    url: '/api/dmc-forget-mocked-local',
    oops: toastError,
    continuation: EffAction (dmcForgetMockedLocalFetchCompleted),
    imsgDecorate: 'Error with DMC forget mocked local call',
  })
}

export function *s_dmcForgetMockedLocalFetchCompleted () {
  yield dmcRepeatSearch () | put
}

export function *s_dmcGetPdfFetch (urn) {
  document.location.href = '/api/dmc-fetch-pdf/' + urn
  // --- ugly to do this with a delay, but need to avoid a race condition where the server hasn't
  // added this PDF to its list of mocked downloads yet.
  yield delay (100)
  // --- easy way to have the results update (so that the rows rearrange based on mocked DMC
  // operations).
  yield dmcRepeatSearch () | put
}

export function *s_dmcIndexKeysFetch (query) {
  yield call (doApiCall, {
    url: '/api/dmc-index-keys',
    resultsModify: map (prop ('data')),
    oops: toastError,
    continuation: EffAction (dmcIndexKeysFetchCompleted),
    imsgDecorate: 'Error getting DMC index keys',
  })
}

export function *s_dmcRepeatSearch () {
  const query = yield selectDmcSearchQuery | select
  if (query | nil) return iwarn ('s_dmcRepeatSearch: query is nil')
  // --- set `reset` to `false` to avoid the results getting cleared and flickering.
  yield dmcSearchFetch (query, false) | put
}

export function *s_dmcSearchFetch ({ query, reset: _, }) {
  yield call (doApiCall, {
    url: '/api/dmc-search',
    optsMerge: {
      method: 'POST',
      body: JSON.stringify ({ data: { query, }, })
    },
    resultsModify: map (prop ('data')),
    oops: toastError,
    continuation: EffAction (dmcSearchFetchCompleted),
    imsgDecorate: 'Error searching DMC',
  })
}

export function *s_dmcSynchronizeFetch () {
  yield delay (3000)
  yield call (doApiCall, {
    url: '/api/dmc-sync',
    oops: toastError,
    continuation: EffAction (dmcSynchronizeFetchCompleted),
    imsgDecorate: 'Error syncing DMC',
  })
}

export function *s_dmcSynchronizeFetchCompleted () {
  yield put (dmcIndexKeysFetch ())
}

export default function *sagaRoot () {
  yield all ([
    saga (takeLatest, dmcFetch, s_dmcFetch),
    saga (takeLatest, dmcForgetMockedLocalFetch, s_dmcForgetMockedLocalFetch),
    saga (takeLatest, dmcForgetMockedLocalFetchCompleted, s_dmcForgetMockedLocalFetchCompleted),
    saga (takeLatest, dmcGetPdfFetch, s_dmcGetPdfFetch),
    saga (takeLatest, dmcIndexKeysFetch, s_dmcIndexKeysFetch),
    saga (takeLatest, dmcRepeatSearch, s_dmcRepeatSearch),
    saga (takeLatest, dmcSearchFetch, s_dmcSearchFetch),
    saga (takeLatest, dmcSynchronizeFetch, s_dmcSynchronizeFetch),
    saga (takeLatest, dmcSynchronizeFetchCompleted, s_dmcSynchronizeFetchCompleted),
  ])
}
