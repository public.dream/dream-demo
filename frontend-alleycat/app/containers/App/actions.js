import {
  pipe, compose, composeRight,
  tap, lets, noop,
} from 'stick-js/es'

import { logWith, } from 'alleycat-js/es/general'
import { action, } from 'alleycat-js/es/redux'

export const dmcFetch = action (
  noop,
  'dmcFetch',
)

export const dmcFetchCompleted = action (
  (rcomplete) => rcomplete,
  'dmcFetchCompleted',
)

export const dmcSynchronizeFetch = action (
  noop,
  'dmcSynchronizeFetch',
)

export const dmcSynchronizeFetchCompleted = action (
  (rcomplete) => rcomplete,
  'dmcSynchronizeFetchCompleted',
)

export const dmcSearchFetch = action (
  (query, reset) => ({ query, reset, }),
  'dmcSearchFetch',
)

export const dmcSearchFetchCompleted = action (
  (rcomplete) => rcomplete,
  'dmcSearchFetchCompleted',
)

export const dmcIndexKeysFetch = action (
  noop,
  'dmcIndexKeysFetch',
)

export const dmcIndexKeysFetchCompleted = action (
  (rcomplete) => rcomplete,
  'dmcIndexKeysFetchCompleted',
)

export const dmcGetPdfFetch = action (
  (urn) => urn,
  'dmcGetPdfFetch',
)

export const dmcGetPdfFetchCompleted = action (
  (rcomplete) => rcomplete,
  'dmcGetPdfFetchCompleted',
)

export const dmcForgetMockedLocalFetch = action (
  noop,
  'dmcForgetMockedLocalFetch',
)

export const dmcForgetMockedLocalFetchCompleted = action (
  (rcomplete) => rcomplete,
  'dmcForgetMockedLocalFetchCompleted',
)

export const dmcRepeatSearch = action (
  noop,
  'dmcRepeatSearch',
)
