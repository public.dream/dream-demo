import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import React, { Fragment, useCallback, useEffect, useRef, useState, } from 'react'

import { connect, useDispatch, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'
import styled from 'styled-components'

import { Nothing, Just, fold, cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import {} from 'alleycat-js/es/dom'
import { mapX, iwarn, ierror, logWith, setTimeoutOn, } from 'alleycat-js/es/general'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { ierrorError, errorError, } from 'alleycat-js/es/react-s-alert'
import { useReduxReducer, useSaga, } from 'alleycat-js/es/redux-hooks'
import { media, mediaQuery, } from 'alleycat-js/es/styled'

import {} from '../App/actions'
import {} from '../../slices/app/selectors'
import {} from '../../slices/domain/selectors'

import saga from './saga'

import { spinner, } from '../../alleycat-components'
import { Button, H1, Input, } from '../../components/shared'
import Main from '../../components/Main'

import { container, useWhy, mediaPhone, mediaTablet, mediaDesktop, mediaTabletWidth, requestResults, } from '../../common'
import config from '../../config'

import {} from '../../types'

const configTop = config | configure.init

const dispatchTable = {
}

const selectorTable = {
}

export default container (
  ['Scenario2', dispatchTable, selectorTable],
  (props) => {
    const {
      isMobile,
    } = props

    useSaga ({ saga, key: 'Scenario2', })

    useWhy ('Scenario2', props, {})

    return <Main title='scenario 2'>
    </Main>
  }
)
