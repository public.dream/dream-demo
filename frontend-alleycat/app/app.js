import {
  pipe, compose, composeRight,
  lets,
  mapValues,
} from 'stick-js/es'

/* For generators (regenerator runtime) and built-ins like 'Reflect'.
 * In the @babel/preset-env section of the babel config, set 'useBuiltIns' to 'entry' and 'corejs'
 * to the right version.
 * core-js will figure out what needs to get polyfilled based on the 'targets' or 'browserslist'
 * options.
 */
import 'core-js/stable'
import 'regenerator-runtime/runtime'

// --- misc. polyfills, e.g. IE Node methods.
import './polyfills'

import './manifest.json'

import './images/icons/favicon.ico'
import './images/icons/icons-192.png'
import './images/icons/icons-512.png'

import React, { Fragment, } from 'react'
import { render, unmountComponentAtNode, } from 'react-dom'
import { Provider, } from 'react-redux'
import { Router, } from 'react-router-dom'

; `
Might need to use hash history (or possibly memory history?) if files will be served using the file:// protocol (e.g. a hybrid app) or from static files (i.e. a build/ directory served by e.g. nginx).
In that case, see if we need 'withRouter' from react-router-dom to fix the 'blocked route updates' problem.
`

import createHistory from 'history/createBrowserHistory'

import 'sanitize.css/sanitize.css'

import { prepareIntl, } from 'alleycat-js/es/react-intl'

/*
// --- css-loader: importing css here makes it available to all components.
import './xxx.css'
*/

import { initStore, } from './redux'
import { GlobalStyle, } from './global-styles'
import { messages, } from './translations'

import { LanguageProvider, } from './containers/LanguageProvider/Loadable'
import { App, } from './containers/App/Loadable'

const initialState = {}

const store = initStore (initialState)
const mountNode = document.getElementById ('app')
const translationMessages = messages | mapValues (prepareIntl)

const history = createHistory ()

const renderApp = (messages) => render (
  <Fragment>
    <Provider store={store}>
      <LanguageProvider messages={messages}>
        <Router history={history}>
          <App history={history}/>
        </Router>
      </LanguageProvider>
    </Provider>
    <GlobalStyle/>
  </Fragment>,
  mountNode,
)

// --- leads to a warning about Router history, but works.
if (module.hot) {
  module.hot.accept (
    './containers/App',
    () => {
      unmountComponentAtNode (mountNode)
      renderApp (translationMessages)
    },
  )
  module.hot.accept ()
}

renderApp (translationMessages)

const { OFFLINE_PLUGIN, } = process.env
const doOffline = OFFLINE_PLUGIN | JSON.parse

if (doOffline)
  require ('offline-plugin/runtime').install ()
