import {
  pipe, compose, composeRight,
  map, dot, dot1, whenOk, ifOk,
  gt, ok, ne, eq,
  prop,
  divideBy, minus,
  whenFalsy, lets,
  defaultToV,
  head,
  take,
  tap, always,
  ifPredicate,
  reduce, sprintfN,
  appendToM, mergeToM, id,
} from 'stick-js/es'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

import { fold, toJust, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { logWith, ierror, reduceX, } from 'alleycat-js/es/general'

import { initialState, } from './reducer'

import { initSelectors, } from '../../common'

const { select, selectTop, selectVal, } = initSelectors (
  'app',
  initialState,
)

export const selectLocale = selectVal ('locale')
