import {
  pipe, compose, composeRight,
  ok, whenOk, dot,
  drop, assoc,
  tap, eq,
  condS, guard, otherwise, guardV,
  id,
  always,
  prop, path,
  merge,
} from 'stick-js/es'

import { logWith, } from 'alleycat-js/es/general'
import { makeReducer, } from 'alleycat-js/es/redux'

import { reducer, } from '../../common'

export const initialState = {
}

const reducerTable = makeReducer (
)

export default reducer ('ui', initialState, reducerTable)
