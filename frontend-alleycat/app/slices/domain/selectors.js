// ------ sending a 'slice' parameter to a selector function is a way to get it to accept a more
// specific chunk of the reducer: useful for calling the selector from the reducer.

import {
  pipe, compose, composeRight,
  map, dot, dot1, whenOk, ifOk,
  gte, ok, ne, eq,
  prop,
  divideBy, minus,
  whenFalsy, lets,
  defaultToV,
  head,
  take,
  tap,
  ifPredicate,
  reduce, sprintfN,
  appendToM, mergeToM, id, T, F,
  bindProp,
  timesF, always,
  die,
  last, not,
  invoke,
  recurry,
  path,
} from 'stick-js/es'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

import memoize1 from 'memoize-one'

import {} from '../../types'

import { fold, toJust, cata, } from 'alleycat-js/es/bilby'
import { info, logWith, ierror, reduceX, min, max, roundUp, slice, } from 'alleycat-js/es/general'
import { ifEmptyList, } from 'alleycat-js/es/predicate'

import { initialState, } from './reducer'

import { initSelectors, } from '../../common'

const { select, selectTop, selectVal, } = initSelectors (
  'domain',
  initialState,
)

export const selectError = selectVal ('error')

export const selectDmcIndexKeys = selectVal ('dmcIndexKeys')
export const selectDmcSearchQuery = selectVal ('dmcSearchQuery')
export const selectDmcSearchResults = selectVal ('dmcSearchResults')
export const selectDmcSynchronize = selectVal ('dmcSynchronize')
export const selectDmcTest = selectVal ('dmcTest')
