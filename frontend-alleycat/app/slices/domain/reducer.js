import {
  pipe, compose, composeRight,
  assoc,
  reduceRight,
  recurry,
  id, always, tap, noop, ifTrue,
  lets,
  prop,
  arg1,
  map,
  take, head,
  mergeTo,
  concat,
  condS, eq, guard, otherwise,
  precat, die, defaultTo, sprintf1,
  dot2,
  timesV,
  mergeM,
  whenPredicate,
  gt,
  divideBy,
  minus,
  deconstruct2,
  plus, update,
  concatTo, precatTo,
  modulo,
  invoke, reduceRightC,
} from 'stick-js/es'

import { foldJust, Just, Nothing, cata, fold, toJust, foldWhenJust, } from 'alleycat-js/es/bilby'
import { RequestInit, RequestLoading, RequestError, RequestResults, } from 'alleycat-js/es/fetch'
import { logWith, nAtATime, } from 'alleycat-js/es/general'
import { makeReducer, } from 'alleycat-js/es/redux'

import {
  dmcFetch, dmcFetchCompleted,
  dmcIndexKeysFetch, dmcIndexKeysFetchCompleted,
  dmcSearchFetch, dmcSearchFetchCompleted,
  dmcSynchronizeFetch, dmcSynchronizeFetchCompleted,
} from '../../containers/App/actions'

import { reducer, } from '../../common'
import {} from '../../types'

export const initialState = {
  // --- `error=true` means the reducer is totally corrupted and the app should halt.
  error: false,
  dmcIndexKeys: RequestInit,
  dmcSearchQuery: null,
  dmcSearchResults: RequestInit,
  dmcSynchronize: RequestInit,
  dmcTest: RequestInit,
}

const resolveRequestComplete = cata ({
  RequestCompleteError: (mbUsg) => RequestError (mbUsg | foldWhenJust (id)),
  RequestCompleteSuccess: (res) => RequestResults (res),
})

const reducerTable = makeReducer (
  dmcFetch, () => assoc ('dmcTest', RequestLoading (Nothing)),
  dmcFetchCompleted, (rcomplete) => assoc (
    'dmcTest', rcomplete | resolveRequestComplete,
  ),
  dmcIndexKeysFetch, () => assoc ('dmcIndexKeys', RequestLoading (Nothing)),
  dmcIndexKeysFetchCompleted, (rcomplete) => assoc (
    'dmcIndexKeys', rcomplete | resolveRequestComplete,
  ),
  dmcSearchFetch, ({ query, reset=true, }) => [
    assoc ('dmcSearchQuery', query),
    reset ? assoc ('dmcSearchResults', RequestLoading (Nothing)) : id,
  ],
  dmcSearchFetchCompleted, (rcomplete) => assoc (
    'dmcSearchResults', rcomplete | resolveRequestComplete,
  ),
  dmcSynchronizeFetch, () => assoc ('dmcSynchronize', RequestLoading (Nothing)),
  dmcSynchronizeFetchCompleted, (rcomplete) => assoc (
    'dmcSynchronize', rcomplete | resolveRequestComplete,
  )
)

export default reducer ('domain', initialState, reducerTable)
