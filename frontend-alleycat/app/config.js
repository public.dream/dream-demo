import {
  pipe, compose, composeRight,
  join, take, id, lets,
  tap,
} from 'stick-js/es'

import { fontFace, cssFont, } from 'alleycat-js/es/font'
import { logWith, } from 'alleycat-js/es/general'

import { envIsDev, envIsTst, envIsNotPrd, } from './env'

// const debugRenders = envIsDev
// const debugReducers = envIsDev
// const debugSelectors = envIsDev
const debugRenders = envIsTst
const debugReducers = envIsTst
const debugSelectors = envIsTst

import imgDownload from './images/icon-download.png'
import imgDownloadRed from './images/icon-download-red.png'
import imgSciHubRaven from './images/sci-hub-raven.png'
import imgView from './images/icon-view.png'

const getMainFontCss = () => join ('\n\n', [
  fontFace (
    'Lora',
    [
      [
        require ('./fonts/lora.woff2'),
        'woff',
      ],
    ],
    {
      weight: 'normal',
      style: 'normal',
      stretch: 'normal',
    },
  ),
])

export default {
  debug: {
    render: debugRenders && {
      Main: true,
    },
    reducers: debugReducers && {
      domain: true,
    },
    selectors: debugSelectors && {
      domain: {
        error: true,
      },
    },
  },
  font: {
    main: {
      css: getMainFontCss (),
      family: 'Lora',
    },
  },
  images: {
    icons: {
      download: imgDownload,
      'download-red': imgDownloadRed,
      view: imgView,
    },
    sciHubRaven: imgSciHubRaven,
  },
  colors: {
    header: {
      color1: '#A62900',
      color2: '#00A629',
    }
  },
}
