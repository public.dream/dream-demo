import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import React, { Fragment, memo, useCallback, useEffect, useRef, useState, } from 'react'

import styled from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { logWith, mapX, } from 'alleycat-js/es/general'
import {} from 'alleycat-js/es/predicate'
import {} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { spinner, } from '../../alleycat-components'
import { Button, H1, Input, } from '../../components/shared'

import { useWhy, mediaPhone, mediaTablet, mediaDesktop, component, } from '../../common'
import config from '../../config'

const configTop = config | configure.init

export default component (
  ['XXX', null],
  (props) => {
    const {} = props
    useWhy ('XXX', props)
    return <div>
    </div>
  },
)
