import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import React, { Fragment, memo, useCallback, useEffect, useRef, useState, } from 'react'

import styled from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { logWith, mapX, } from 'alleycat-js/es/general'
import {} from 'alleycat-js/es/predicate'
import {} from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { Button, H1, Input, } from '../../components/shared'

import { useWhy, mediaPhone, mediaTablet, mediaDesktop, component, } from '../../common'
import config from '../../config'

const configTop = config | configure.init

const imgSciHubRaven = configTop.get ('images.sciHubRaven')

const LogoS = styled.div`
  font-size: 30px;
`

export const Logo = () => <LogoS>
  DREAM
</LogoS>

const HeaderS = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  .x__left {
    flex: 0 1 20%;
    text-align: center;
  }
  .x__right {
    flex: 0 1 80%;
    text-align: center;
  }
`

const Header = ({ title, }) => <HeaderS>
  <div className='x__left'>
    <img src={imgSciHubRaven} width='100px'/>
  </div>
  <div className='x__right'>
    <Logo/>
    <H1>{title}</H1>
  </div>
</HeaderS>

const MainS = styled.div`
  height: 100%;
  overflow-y: auto;
`

export default component (
  ['Main', null],
  (props) => {
    const { title, children, } = props
    useWhy ('Main', props)
    return <MainS>
      <Header title={title}/>
      {children}
    </MainS>
  },
)
