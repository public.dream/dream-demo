import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import { messages as en, } from './lang/en'
import { messages as nl, } from './lang/nl'

export const defaultMessages = en

export const messages = {
  en,
  nl,
}
