export const messages = {
  'app.containers.Main': {
    fruit: 'Lookz: {num} {num, plural, one {apple} other {apples}}',
    mood: 'I\'m fine',
  },
}
