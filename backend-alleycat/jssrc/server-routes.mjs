import {
  pipe, compose, composeRight,
  map, each, list, fromPairs, ok, anyAgainst,
  appendTo, deconstruct, tap, appendM,
  ifFalse, concat,
  recurry, sprintf1,
  concatTo, ifPredicate, whenPredicate,
  sprintfN, noop, T,
  ifOk, invoke, keys, id,
  mapTuples, path, eq,
  condS, guard, otherwise, values,
  letS, neu1, whenNil, mergeToM, assoc,
  reduce, addIndex2, addIndex,
  eachObj, whenHas,
} from 'stick-js/es'

import pathMod from 'path'
import sortBy from 'ramda/es/sortBy.js'
import groupBy from 'ramda/es/groupBy.js'

import { Left, Right, fold, } from 'alleycat-js/es/bilby'
import { composeManyRight, iwarn, warn, } from 'alleycat-js/es/general'
import { ifEquals, isEmptyObject, ifEmptyList, } from 'alleycat-js/es/predicate'

import {
  use, all, get, post, patch, del, put, send, sendStatus, sendStatusRaw, status,
  sendStatusEmpty, download,
} from './express.mjs'

import { info, warnX, logWith, brightRed, spawnSync, } from './io.mjs'
import { inspect, decorateRejection, doEither, bifirst, bisecond, bimapEither, compactDefined, ifZero, lookupOn, lookupOnOr, __dirname, } from './util.mjs'

const pdfDir = pathMod.join (
  __dirname (import.meta.url), '..', 'mockData', 'pdf',
)

const uniqueValues = (o) => [... new Set (o | values).values ()]
const sort = sortBy (id)
const removeKeyM = recurry (2) (
  (key) => (o) => (delete o [key], o),
)
const { basename, } = pathMod
const arrow2 = recurry (3) (
  (a) => (b) => (o) => [a (o), b (o)]
)
const eachX = addIndex (each)
const reduceX = addIndex2 (reduce)

const setHas = recurry (2) (
  (x) => (s) => s.has (x),
)
const inSet = recurry (2) (
  (s) => (x) => setHas (x, s)
)
// --- the `yes` functions receive the set as argument (the value being
// piped).
const whenSetHas = recurry (3) (
  setHas >> whenPredicate
)
const ifSetHas = recurry (4) (
  setHas >> ifPredicate
)
// --- the `yes` functions receive the element as argument (the value being
// piped).
const whenInSet = recurry (3) (
  inSet >> whenPredicate
)
const ifInSet = recurry (4) (
  inSet >> ifPredicate
)

/**
 * Immutable move the element `xs [i]` to the end of `ys`.
 */
const move = (xs, i, ys) => {
  const el = xs [i]
  return [xs | reduceX (
    (xt, x, n) => n | ifEquals (i) (
      () => xt,
      () => xt | appendM (x),
    ),
    [],
  ), [... ys, el]]
}

const umsg = (umsg) => ({ umsg, })
const imsg = (imsg) => ({ imsg, })
const iumsg = (msg) => ({ imsg: msg, umsg: msg, })
const data = (data) => ({ data, })

const globals = {
  downloaded: { current: new Set (), }
}

const mockResults = () => {
  // --- availability = local / local-area / wide-area
  const artGlobal = {
    __private: {
      pdfPath: pathMod.join (pdfDir, 'approaching.pdf'),
    },
    pdfInfo: {
      availability: 'local',
      erisURN: 'urn:erisx2:BIxxxxxxx1'
    },
    info: {
      title: 'Approaching global education development with a decolonial lens: teachers’ reflections. Teaching in Higher Education',
      doi: '10.1080/13562517.2021.1941845',
      date: 2021,
      author: [
        'Menon, S.',
        'Green, C.',
        'Charbonneau, I.',
        'Lehtomäki, E.',
        'Mafi, B.'
      ],
    },
  }
  const artWitches = {
    __private: {
      pdfPath: pathMod.join (pdfDir, 'witches.pdf'),
    },
    pdfInfo: {
      availability: 'local-area',
      erisURN: 'urn:erisx2:BIxxxxxxx2'
    },
    info: {
      title: 'Witches, Charlatans, and Old Wives: Critical Perspectives on History of Women’s Indigenous Knowledge',
      doi: '10.1080/02703149.2017.1323474',
      date: 2017,
      author: [
        'Yakushko, O.',
      ],
    },
  }
  const artWitches2 = {
    __private: {
      pdfPath: pathMod.join (pdfDir, 'historiography.pdf'),
    },
    pdfInfo: {
      availability: 'wide-area',
      erisURN: 'urn:erisx2:BIxxxxxxx3'
    },
    info: {
      title: 'The Historiography of European Witchcraft: Progress and Prospects',
      doi: '10.2307/202315',
      date: 1972,
      author: [
        'Monter, E.W.',
      ],
    },
  }
  const index = {
    '10.1080/13562517.2021.1941845': artGlobal,
    '10.1080/02703149.2017.1323474': artWitches,
    '10.2307/202315': artWitches2,
    'menon': artGlobal,
    'green': artGlobal,
    'charbonneau': artGlobal,
    'lehtomäki': artGlobal,
    'mafi': artGlobal,
    'yakushko': artWitches,
    'monter': artWitches2,
    'approaching global education development with a decolonial lens': artGlobal,
    'witches, charlatans, and old wives': artWitches,
    'the historiography of european witchcraft': artWitches2,
  }
  return [
    index,
    // --- { eris urn => pdf path }
    [artGlobal, artWitches, artWitches2] | mapTuples (
      (_, art) => [art.pdfInfo.erisURN, art.__private.pdfPath | arrow2 (id, basename)],
    ),
  ]
}

export const routes = ({ dmcSetId, dmcPrivkey, }) => {
  return composeManyRight (
    get ('/dmc-index-keys', (req, res) => {
      const [mockIndex, mockPdfPaths] = mockResults ()
      const theKeys = mockIndex | keys | sort | appendM ('*')
      return res | sendStatus (200, { data: theKeys, })
    }),
    get ('/dmc-fetch-pdf/:urn', (req, res) => {
      const [mockIndex, mockPdfPaths] = mockResults ()
      const urn = req.params.urn
      mockPdfPaths [urn] | ifOk (
        ([path, filename]) => res | download (path, filename, (err) => {
          if (err) return warn ('couldn\'t send pdf for download:', err)
          globals.downloaded.current.add (urn)
        }),
        () => res | sendStatusEmpty (404),
      )
    }),
    get ('/dmc-forget-mocked-local', (req, res) => {
      globals.downloaded.current = new Set ()
      return res | sendStatus (200, null)
    }),
    post ('/dmc-search', (req, res) => {
      const [mockIndex, mockPdfPaths] = mockResults ()
      const query = req.body | path (['data', 'query'])
      if (!query) return res | sendStatus (422, 'missing param query')
      const empty = { local: [], 'local-area': [], 'wide-area': [], }
      const results = query.toLowerCase ()
        | condS ([
          '*' | eq | guard (() => mockIndex | uniqueValues),
          otherwise | guard ((qu) => qu | lookupOn (mockIndex) | ifOk (
            (res) => [res],
            () => [],
          )),
        ])
        | map (removeKeyM ('__private'))
        | groupBy (path (['pdfInfo', 'availability']))
        | mergeToM (empty)

      // --- if a PDF has already been downloaded, move it from local-area /
      // wide-area to the end of local and change its availability to local.
      ; ['local-area', 'wide-area'] | each ((resultsKey) => {
        const ress = results [resultsKey]
        ress | eachX (({ pdfInfo, }, n) => pdfInfo.erisURN | whenInSet (globals.downloaded.current) (() => {
          pdfInfo.availability = 'local'
          ; [results [resultsKey], results.local] = move (ress, n, results.local)
        }))
      })
      res | sendStatus (200, { data: results, })
    }),
    get ('/dmc-sync', (req, res) => {
      /*
       * This is where the routine to sync DMC should go. It will be tricky
       * to implement when this API call should return. We will send an
       * asynchronous request to our upsycle-router, which will communicate
       * with the remote one, which will asynchronously send an update to
       * our DMC. So this call should return only after our DMC has been
       * 'fully updated' -- whatever that means!
       */
       res | sendStatus (200, null)
    }),
    get ('/dmc-test', (req, res) => {
      spawnSync (
        'dmc', ['object_graph', dmcSetId], {
          verbose: true,
        },
      ) | fold (
        () => res | sendStatus (500, {
          imsg: 'see server logs',
          umsg: 'server error calling DMC',
        }),
        (out) => res | sendStatus (200, { data: out, }),
      )
    }),
  )
}
