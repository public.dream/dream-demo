import {
  pipe, compose, composeRight,
  side1, side2, side3, recurry,
} from 'stick-js/es'

export const use = side1 ('use')
export const all = side2 ('all')
export const get = side2 ('get')
export const post = side2 ('post')
export const listen = side2 ('listen')
export const send = side1 ('send')
// --- send JSON.
export const sendJSON = side1 ('json')
// --- send null body.
export const sendEmpty = send (null)
// --- only set status.
export const status = side1 ('status')
// --- set status and send a string.
export const sendStatusRaw = recurry (3) (
  (code) => (body) => status (code) >> send (body),
)
// --- set status and send JSON.
export const sendStatus = recurry (3) (
  (code) => (data) => status (code) >> sendJSON (data),
)
export const sendStatusEmpty = recurry (2) (
  (code) => status (code) >> sendEmpty,
)
export const patch = side2 ('patch')
// --- `delete` is a keyword.
export const del = side2 ('delete')
export const put = side2 ('put')
export const download = side3 ('download')
