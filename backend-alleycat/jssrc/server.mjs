import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry, map, split, join,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN,
  noop, always, T, F,
  prop, path, lets, letS,
  die, raise, not, ifTrue, ifFalse,
  anyAgainst, whenTrue, againstAny,
  lt, gt, eq, ne, lte, gte,
  passToN, invoke, arg0, mergeM,
  factory, factoryProps,
  bindTo, each, find,
} from 'stick-js/es'

import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'

import configure from 'alleycat-js/es/configure'
import { then, recover, resolveP, } from 'alleycat-js/es/async'
import { setTimeoutOn, } from 'alleycat-js/es/general'

import { config, } from './config.mjs'
import { send, sendStatus, use, all, listen, } from './express.mjs'
import { routes, } from './server-routes.mjs'
import {
  eventEventData,
} from './types.mjs'

import { whenMapKeyExistsOn, lookupOn, } from './util.mjs'

const configTop = config | configure.init

// --- @todo
const timeout = 5000

const getReqParams = prop ('params') >> lookupOn

/* Returns `constructor` or false.
 */
const eventIs = recurry (2) (
  (constructor) => (constructed) => constructor.is (constructed) ? constructor : false,
)

/* Returns `constructor` or false.
 */
const eventIsAny = recurry (2) (
  (constructors) => (constructed) => constructors | find (
    (constructor) => eventIs (constructor, constructed),
  ),
)

// --- takes a list of events and their associated callbacks. Once any one of the events fired, theh
// entire group is removed from the map.
const getOnce = (onceMap) => (eventSpecs) => {
  eventSpecs | each (
    ([eventType, cb]) => onceMap.set (eventType, cb),
  )
  return () => eventSpecs | each (
    ([eventType, ..._]) => onceMap.delete (eventType),
  )
}

/* `once`: a function of (eventType, callback) which registers a callback for an event which will
 * be called the next time the event is called and then never again. It returns a function which can
 * be used to clear the event listener before the event is fired.
 * `eventSpecs`: a list of tuples of [`eventType`, `mkResponse`], where:
 *   `eventType`: the event type of the event we are listening for.
 *   `mkResponse`: a function for creating the response body.
 */
const eventEndpoint = (once, eventSpecs, cleanup, res) => {
  let sent = false
  let clearOnce

  timeout | setTimeoutOn (() => {
    if (sent) return
    clearOnce ()
    cleanup ()
    res | sendStatus (499, {
      umsg: 'Timed out waiting for read.',
    })
  })

  // :: event payload -> Response
  const successCb = (mkResponse) => (eventData) => {
    clearOnce ()
    sent = true
    cleanup ()
    res | send (mkResponse (eventData))
  }
  clearOnce = once (eventSpecs | map (
    ([eventType, mkResponse]) => [eventType, successCb (mkResponse)],
  ))
}

const corsOptions = {
  // --- reflect the request origin.
  origin: true,
  // --- allow credentials mode 'include' in the request.
  credentials: true,
  methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
  preflightContinue: false,
  optionsSuccessStatus: 204,
}

const proto = {
  init ({ port, onReady, }) {
    const { dmcSetId, dmcPrivkey, } = this
    express ()
    | use (cors (corsOptions))
    | use (bodyParser.json ())
    | all ('*', (req, res, next) => {
      next ()
    })
    | routes ({ dmcSetId, dmcPrivkey, })
    | listen (port) (() => onReady (port))

    return this
  },

  // --- called on every event which is fired.
  handleEvent (event) {
  },
}

const props = {
  dmcSetId: null,
  dmcPrivkey: null,
}

export default proto | factory | factoryProps (props)
