#!/usr/bin/env node

import {
  pipe, compose, composeRight,
  tryCatch,
  map, sprintf1, decorateException,
  xReplace,
} from 'stick-js/es'

import fs from 'fs'
import yargsMod from 'yargs'

import configure from 'alleycat-js/es/configure'
import { toString, } from 'alleycat-js/es/general'

import { config, } from './config.mjs'
import { fireEventOnTarget, } from './event.mjs'

import {
  log, info, infoSecret, warn, error, errorPlain, errorX,
  green,
  spawnSync, logWith,
  init as initIO,
  cleanup as cleanupIO,
} from './io.mjs'

import serverMod from './server.mjs'

import {
  Server, Event, EventHandler,
} from './types.mjs'

process.title = 'dream-demo-backend'

const configTop = config | configure.init

const chomp = xReplace (/\n$/s, '')
const slurp = fs.readFileSync >> toString >> chomp

const yargs = yargsMod
  .usage ('Usage: node $0 [options]')
  .strict ()
  .help ('h')
  .alias ('h', 'help')
  .option ('dmc-set-id-file', {
    string: true,
  })
  .option ('dmc-privkey-file', {
    string: true,
  })
  .coerce (['dmc-set-id-file', 'dmc-privkey-file'], slurp)
  .demandOption (['dmc-set-id-file', 'dmc-privkey-file'])
  .showHelpOnFail (false, 'Specify --help for available options')

const serverPort = process.env.PORT || configTop.get ('serverPort')

const opt = yargs.argv
// --- showHelp also quits.
if (opt._.length !== 0)
  yargs.showHelp (errorPlain)

initIO (false, false, 0)

const sigint = () => {
  info ('sigint')
  cleanupIO ()
  exit (0)
}

const exit = (rc) => {
  info ('doei')
  cleanupIO ()
  process.exit (rc)
}

const go = () => {
  const dmcSetId = opt ['dmc-set-id-file']
  const dmcPrivkey = opt ['dmc-privkey-file']

  info ('dmcSetId', dmcSetId)
  infoSecret ('dmcPrivkey', dmcPrivkey)

  const initServer = () => {
    const port = serverPort
    const serverM = serverMod.create ({
      dmcSetId,
      dmcPrivkey,
    }).init ({
      port,
      // :: Number |port| -> IO ()
      onReady: String >> green >> sprintf1 ('listening on port %s') >> info,
    })
    const handleEvent = (event, ..._) => serverM.handleEvent (event)
    const handlers = []
    return Server (serverM, handlers)
  }

  const initEvents = (targets) => {
    const fireAll = (event) => targets | map (
      fireEventOnTarget (event),
    )
    return Event >> fireAll
  }

  const server = tryCatch (
    (server) => {
      info ('server ready', green ('✔'))
      return server
    },
    decorateException ("Couldn't init server:") >> errorX,
    () => initServer (),
  )

  const fireEvent = tryCatch (
    (fire) => {
      info ('events ready', green ('✔'))
      return fire
    },
    decorateException ("Couldn't init events:") >> errorX,
    () => initEvents ([server]),
  )
}

go ()
