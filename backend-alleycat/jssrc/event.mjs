/* Each Led structure has a list of 0 or more events it's interested in. Here we send events to all
 * Leds that are interested in them.
 */


import {
  pipe, compose, composeRight,
  map, recurry, whenPredicate,
  lets,
} from 'stick-js/es'

import { whenEq, } from './util.mjs'

import {
  getEventHandlers, eventHandlerCb, eventHandlerType,
  eventEventData,
} from './types.mjs'

const isHandledBy = recurry (2) (
  (handler) => (event) => lets (
    () => handler | eventHandlerType,
    () => event | eventEventData,
    (constructor, constructed) => constructor.is (constructed),
  ),
)

const whenIsHandledBy = (handler) => isHandledBy (handler) | whenPredicate

/* Given an event target and an event type, cycle through the list of event types associated with the
 * event target. For each one that matches, run the callback associated with the event.
 */

// :: (EventType, {}) -> {EventTarget} -> {CallbackResult}
export const fireEventOnTarget = (event) => (target) => target | getEventHandlers | map (
  (handler) => lets (
    () => handler | eventHandlerCb,
    (cb) => event | whenIsHandledBy (handler) (
      () => cb (event, target),
    ),
  ),
)
