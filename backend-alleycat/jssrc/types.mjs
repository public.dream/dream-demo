import {
  prop, dot,
} from 'stick-js/es'

import daggy from 'daggy'

import { Just, Nothing, cata, } from 'alleycat-js/es/bilby'

import { logWith, info, green, red, } from './io.mjs'
import { whenEq, N, lookupOnOrDie, } from './util.mjs'

/* `serverM` is an object created by the server factory in server.js -- M for mutable.
 */
// :: data Server = Server { serverM :: ServerFactoryObject
//                         , eventHandlers :: [EventHandler] }
export const Server = daggy.tagged ('Server', ['serverM', 'eventHandlers'])
export const serverServerM = prop ('serverM')
export const serverEventHandlers = prop ('eventHandlers')

// :: data EventData = EventExample (Number)

export const EventData = daggy.taggedSum ('EventData', {
  EventExample: ['n'],
})

export const {
  EventExample,
} = EventData

// :: data EventHandler = EventHandler { eventHandlerType :: EventDataConstructor,
//                                     , eventHandlerCb :: (->) }
export const EventHandler = daggy.tagged ('EventHandler', ['type', 'cb'])

export const eventHandlerType = prop ('type')
export const eventHandlerCb = prop ('cb')

// :: data Event = Event { eventEventData :: EventData }
export const Event = daggy.tagged ('Event', ['eventData'])
export const eventEventData = prop ('eventData')

Server.prototype.getEventHandlers = function () {
  return this | serverEventHandlers
}

export const getEventHandlers = dot ('getEventHandlers')
