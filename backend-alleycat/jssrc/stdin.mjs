import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry,
  map, split,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  bindProp, sprintf1, sprintfN,
  noop, always, T, F,
  prop, path, lets, letS,
  die, raise, notOk,
} from 'stick-js/es'

import readline from 'readline'

const on = side2 ('on')
const setRawMode = side1 ('setRawMode')
const emitKeypressEvents = readline | bindProp ('emitKeypressEvents')
const whenNotTTY = whenPredicate (prop ('isTTY') >> notOk)
const checkTTY = whenNotTTY (() => die (
  'Standard input is not a TTY.',
))

/* @throws on no TTY
 */
export const init = (handleKeyPress) => process.stdin
  | tap (checkTTY)
  | tap (emitKeypressEvents)
  | setRawMode (true)
  | on ('keypress', handleKeyPress)
