FROM debian:bullseye-slim AS base
RUN apt update
RUN apt install -y --no-install-recommends \
  apt-transport-https pkg-config m4 \
  git \
  opam
RUN apt install -y --no-install-recommends \
  aptitude screen vim-nox \
  iputils-ping procps curl

FROM base AS ocaml-4.11.0
# --- used in opam deps
RUN apt install -y --no-install-recommends \
  libgmp-dev libffi-dev zlib1g-dev
RUN opam init -y --disable-sandboxing --compiler=4.11.0
RUN opam update
RUN opam install -y dune
# --- try to pull in some useful packages
RUN opam install -y cohttp containers lwt ppxlib utop

FROM ocaml-4.11.0 as upsycle
RUN mkdir /upsycle
COPY sources/ocaml-seaboar /upsycle/ocaml-seaboar
COPY sources/ocaml-conduit-dream /upsycle/ocaml-conduit-dream
COPY sources/upsycle-router /upsycle/upsycle-router
RUN cd /upsycle/ocaml-seaboar && opam install -y --with-test .
RUN cd /upsycle/ocaml-conduit-dream && opam install -y \
  ./conduit-lwt-unix-dream.opam ./conduit-lwt-dream.opam ./conduit-dream.opam
RUN cd /upsycle/upsycle-router && opam install -y --with-test .

# --- this is where we will build rem (which links with upsycle-router)
FROM upsycle as dream

FROM ocaml-4.11.0 as dmc
# --- z3
RUN apt install -y --no-install-recommends \
  python3
# --- ocaml-base32
RUN opam install -y qcheck qcheck-alcotest
# --- ocaml-rdf
RUN opam install -y uuidm z3 uunf xmlm
RUN apt install -y --no-install-recommends \
  libserd-dev
RUN opam install -y \
  containers cryptokit datalog alcotest-lwt \
  irmin.2.6.1 irmin-pack.2.6.1 irmin-unix.2.6.1
RUN mkdir /dromedar
COPY sources/ocaml-base32 /dromedar/ocaml-base32
COPY sources/ocaml-monocypher /dromedar/ocaml-monocypher
COPY sources/ocaml-eris /dromedar/ocaml-eris
COPY sources/ocaml-cbor /dromedar/ocaml-cbor
COPY sources/ocaml-rdf /dromedar/ocaml-rdf
COPY sources/ocaml-dmc /dromedar/ocaml-dmc
RUN eval $(opam env) && cd /dromedar/ocaml-base32 && dune build && dune install
RUN eval $(opam env) && cd /dromedar/ocaml-monocypher && dune build && dune install
RUN eval $(opam env) && cd /dromedar/ocaml-eris && dune build && dune install
RUN eval $(opam env) && cd /dromedar/ocaml-cbor && dune build && dune install
RUN eval $(opam env) && cd /dromedar/ocaml-rdf && dune build && dune install
RUN eval $(opam env) && cd /dromedar/ocaml-dmc && dune build && dune install

FROM base as node-and-ruby
COPY gpg/nodesource.gpg.key /nodesource.gpg.key
RUN echo 'deb [signed-by=/nodesource.gpg.key] https://deb.nodesource.com/node_16.x bullseye main' > /etc/apt/sources.list.d/nodesource.list
RUN apt update
RUN apt install -y --no-install-recommends \
  nodejs='16.*'
RUN corepack enable
RUN apt install -y --no-install-recommends \
  rbenv \
  libssl-dev zlib1g-dev
RUN rbenv init -
RUN cd "$HOME"/.rbenv && mkdir plugins
RUN cd "$HOME"/.rbenv/plugins && git clone https://github.com/rbenv/ruby-build.git
RUN rbenv install 3.1.0
RUN rbenv global 3.1.0

FROM node-and-ruby as node-and-ruby-with-sinatra-and-deps
COPY sources/dream-demo-sinatra.tar /
RUN tar xf dream-demo-sinatra.tar
RUN rm -f dream-demo-sinatra.tar
# --- @todo .ruby-version file contains '3.1', which causes both 3.1.0 and 3.1.1 to fail
RUN rm -f /dream-demo-sinatra/.ruby-version
RUN eval "$(rbenv init -)" && cd /dream-demo-sinatra && bundle
# --- @todo we seem to need one of thin,puma,reel,HTTP,webrick -- which is best?
RUN eval "$(rbenv init -)" && gem install thin
RUN eval "$(rbenv init -)" && gem install haml

FROM node-and-ruby as frontend-alleycat
COPY sources/frontend-alleycat.tar /
RUN tar xf frontend-alleycat.tar
RUN rm -f frontend-alleycat.tar
RUN cd /frontend-alleycat && bin/build

FROM node-and-ruby as backend-alleycat
# RUN mkdir /backend
COPY sources/backend-alleycat.tar /backend-alleycat.tar
RUN tar xf backend-alleycat.tar
RUN rm -f backend-alleycat.tar
RUN cd /backend-alleycat && bin/build

FROM node-and-ruby-with-sinatra-and-deps AS final
RUN apt install -y --no-install-recommends \
  nginx
COPY config/nginx/nginx.conf /etc/nginx/nginx.conf
COPY config/nginx/nginx-deploy.conf /etc/nginx/sites-enabled/dream.conf
COPY --from=dmc /root/.opam/4.11.0/bin/dmc /usr/local/bin/dmc
COPY --from=upsycle /root/.opam/4.11.0/bin/upsycle-router /usr/local/bin/upsycle-router
COPY --from=node-and-ruby-with-sinatra-and-deps /dream-demo-sinatra /demo-sinatra
COPY --from=frontend-alleycat /frontend-alleycat/build/prd /frontend-alleycat
COPY --from=backend-alleycat /backend-alleycat /backend-alleycat
COPY --from=upsycle /upsycle/upsycle-router /upsycle-router
COPY config/upsycle-router/conf.yaml /upsycle-router.yaml
COPY libs/alleycat-bash/functions.bash /functions.bash
COPY dmc-set-init-parse.py /dmc-set-init-parse.py
COPY entrypoint.sh /

CMD ["/bin/bash", "/entrypoint.sh"]

# FROM demo-sinatra
# CMD ["/bin/bash"]
