#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

USAGE="Usage: $0 { init | get-article DOI }"

set_id_file=dmc-pdf-set-id
privkey_file=dmc-pdf-privkey

_init-dmc () {
  local set_id
  local privkey
  read set_id
  read privkey
  info "[dmc] set id: $set_id"
  info "[dmc] privkey: $privkey"
  cmd redirect-out "$set_id_file" echo "$set_id"
  cmd redirect-out "$privkey_file" echo "$privkey"
}

init-dmc () {
  local output
  pipe-capture _ret ./dmc-set-init-parse.py dmc set_init
  fun _init-dmc <<< "$_ret"
}

get-article () {
  local doi=$1
  local doih
  doih=$(echo "$doi" | sha256sum | cut -d' ' -f1)
  cmd redirect-out "$doih".rdf curl -LH 'Accept: text/turtle' http://doi.org/"$doi"
  # curl -o $doih.pdf https://sci-hub.st/downloads/2021-08-10/5e/menon2021.pdf?download=true
  info "go to https://sci-hub.st/$doi and do
  curl -o $doih.pdf ..."
}

if [ "$1" = init ]; then
  fun init-dmc
elif [ "$1" = get-article ]; then
  shift
  fun get-article "$@"
else
  error "$USAGE"
fi
