sources_dir_parts=("$rootdir" sources)
sources_dir=$(join-out / sources_dir_parts)
declare -A repos
declare -A revisions
repos=(
  ocaml-base32 https://gitlab.com/public.dream/dromedar/ocaml-base32
  ocaml-cbor http://inqlab.net/git/ocaml-cbor.git
  ocaml-eris https://gitlab.com/public.dream/dromedar/ocaml-eris
  ocaml-monocypher https://gitlab.com/public.dream/dromedar/ocaml-monocypher
  ocaml-rdf https://gitlab.com/public.dream/dromedar/ocaml-rdf
  ocaml-dmc https://gitlab.com/public.dream/dromedar/ocaml-dmc
  ocaml-seaboar https://gitlab.com/public.dream/libs/ocaml-seaboar
  upsycle-router https://gitlab.com/public.dream/UPSYCLE/upsycle-router
  ocaml-conduit-dream https://gitlab.com/public.dream/upsycle/ocaml-conduit-dream
  dream-demo-sinatra git@gitlab.com:public.dream/dream-demo-sinatra
)
revisions=(
  # --- e.g.
  # ocaml-base32 <some-commit>
  # ocaml-monocypher origin/master
  # (leave blank for latest commit)
)

container_nginx_alleycat_port=8080
container_nginx_sinatra_port=8081
container_upsycle_port=7000
container_upsycle_control_port=7001
