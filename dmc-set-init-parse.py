#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import sys

def strip_colors(x):
  return re.sub('\x1b\\[\\d+?m', '', x)

'''
Set identifier: dmc:AAAP6QGL6WT2X4CVKJ53ZSEHTSTIAAGLOZLS64KL4NCGJ5ARITEC26OJY3SGZP7NPBED4AZIUC47T3WAUYO5ZDJHMNNGQWUJCEY5KIT7JY
Secret key (do not loose or share!): urn:ed25519:sk:6QTCQEL33SPPFUGVCRPHQ6BB6OMFAC4733NCAXYDOFWHEU7ETB4Q
'''

def go():
  s = sys.stdin.read()
  assert s !=  ''
  t = strip_colors(s)
  m0 = re.search(r'Set \s identifier: \s+ (.+)', t, re.X)
  assert m0 is not None
  set_id = m0[1]
  m1 = re.search(r'Secret \s key.+: \s+ (.+)', t, re.X)
  assert m1 is not None
  privkey = m1[1]
  return set_id, privkey

set_id, privkey = go()
print(set_id)
print(privkey)
