#!/bin/bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")
. "$bindir"/functions.bash

dmcindexfile=/root/.local/share/dmc/init
set_id_file=/dmc-set-id
privkey_file=/dmc-privkey

_ret=0

wait-file () {
  local file
  for file in "$@"; do
    while [ ! -e "$file" ]; do
      sleep 1
    done
  done
}

_init-dmc () {
  local set_id
  local privkey
  read set_id
  read privkey
  info "set id: $set_id"
  info "privkey: $privkey"
  cmd redirect-out "$set_id_file" echo "$set_id"
  cmd redirect-out "$privkey_file" echo "$privkey"

  cmd-print "echo 'hello from dreamz' | dmc set_add_binary ..."
  cmd dmc set_add_binary --secret-key "$privkey" "$set_id" <<< 'hello from dreamz'
}

init-dmc () {
  local output
  pipe-capture _ret ./dmc-set-init-parse.py dmc set_init
  fun _init-dmc <<< "$_ret"
}

dmc-initted () {
  test -e "$dmcindexfile"
}

do-dmc () {
  if ! fun dmc-initted; then
    fun init-dmc
  fi
}

do-demo-sinatra () {
  eval "$(rbenv init -)"
  cwd /demo-sinatra ruby app.rb
}

do-backend-alleycat () {
  cmd node /backend-alleycat --dmc-set-id-file "$set_id_file" --dmc-privkey-file "$privkey_file"
}

do-upsycle-router () {
  mkd /upsycle-router-keys
  # --- @todo expiration
  cwd /upsycle-router bin/gen-keys-and-certs -d /upsycle-router-keys -n 365 message-router
  cmd upsycle-router -c /upsycle-router.yaml
}

do-nginx () {
  cmd rm -f /etc/nginx/sites-enabled/default
  cmd rm -f /var/log/nginx/*.log
  cmd /usr/sbin/nginx -g 'daemon off; master_process on;'
}

log-nginx () {
  local which=$1
  tail -f /var/log/nginx/"$which".log
}

tag () {
  local tag=$1; shift
  local line
  "$@" | while read line; do
    echo "$tag $line"
  done
}

tag "$(yellow dmc)" do-dmc
tag "$(cyan demo-sinatra)" do-demo-sinatra &
tag "$(green backend-alleycat)" do-backend-alleycat &
tag "$(magenta upsycle-router)" do-upsycle-router &
do-nginx &
fun wait-file /var/log/nginx/{access,error}.log
cmd sleep 5
tag "$(bright-blue nginx)" log-nginx access &
tag "$(bright-red nginx)" log-nginx error &
sleep infinity
