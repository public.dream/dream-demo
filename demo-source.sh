# Source this file in your shell for the following commands
echo -n "Sourcing demo commands: dmc-cmd and run-cmd."

# dmc-cmd sends commands to the DMC replica in the container.
# Try: dmc-cmd --help for help
# Get SetID from the docker log and run the Datalog REPL:
# dmc-cmd repl $SetID
dmc-cmd() { docker exec -it dream-demo dmc "$@"; }
echo -n "."

# run-cmd sends orders to the control port of the Router for UPSYCLE Network
# Try: run-cmd h for help
run-cmd() { nc localhost 44445 <<< "$1"; }
echo -n "."

echo " Done. Try running 'dmc-cmd --help' or 'run-cmd h'"
