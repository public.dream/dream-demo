#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")
rootdir="$bindir"/..

. "$bindir"/functions.bash
. "$rootdir"/vars.sh
. "$rootdir"/vars-local.sh

USAGE="Usage: $0 { init | build [-C] (-C = disable Docker cache) | update-sources | run [-d] <arg> [...<arg>] (-d = detach)}"

while getopts h-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        -)  OPTARG_KEY="${OPTARG%=*}"
            OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG_KEY in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

update-repo () {
  local revision=$1
  if [ -n "$revision" ]; then
    cmd git fetch
    cmd git reset --hard "$revision"
  else
    cmd git pull
  fi
  cmd git submodule update --init --recursive
}

update-source () {
  local dirname=$1
  local url=$2
  local revision=$3
  if [ ! -d "$dirname" ]; then
    cmd git clone "$url" "$dirname"
  fi
  cwd "$dirname" fun update-repo "$revision"
}

prepare-sources-dir () {
  safe-rm-dir-array sources_dir
  mkd "$sources_dir"
}

_update-sources () {
  local key
  local url
  local revision
  for key in "${!repos[@]}"; do
    url=${repos["$key"]}
    revision=${revisions["$key"]:-}
    fun update-source "$key" "$url" "$revision"
  done
}

update-sources () {
  cwd "$sources_dir" fun _update-sources
}

package-sinatra-app () {
  mci
  mcb tar cf dream-demo-sinatra.tar
  mcb   dream-demo-sinatra
  mcg
}

# --- speed up copy by excluding node_modules (these are recreated when
# `yarn` is run, and .yarn/cache is used if it exists).

package-frontend-alleycat () {
  mci
  mcb tar cf "$sources_dir"/frontend-alleycat.tar
  mcb   --dereference
  mcb   --exclude=node_modules
  mcb   frontend-alleycat
  mcg
}

package-backend-alleycat () {
  mci
  mcb tar cf "$sources_dir"/backend-alleycat.tar
  mcb   --dereference
  mcb   --exclude=node_modules
  mcb   --exclude=jslib
  mcb   backend-alleycat
  mcg
}

build-image () {
  local disable_cache=$1
  mci
  mcb docker build .
  mcb   -t "$docker_image_name"
  if [ "$disable_cache" = yes ]; then
  mcb   --no-cache
  fi
  mcg
}

cmd-init () {
  fun prepare-sources-dir
  fun update-sources
}

cmd-build () {
  local opt_C=
  while getopts C-: arg; do
      case $arg in
          C) opt_C=yes ;;
          -)  OPTARG_KEY="${OPTARG%=*}"
              OPTARG_VALUE="${OPTARG#*=}"
              case $OPTARG_KEY in
                  help)  warn "$USAGE"; exit 0 ;;
                  '')    break ;;
                  *)     error "Illegal option --$OPTARG" ;;
                  esac ;;
          *) error "$USAGE" ;;
      esac
  done
  shift $((OPTIND-1))

  cwd "$sources_dir" fun package-sinatra-app
  cwd "$rootdir" fun package-frontend-alleycat
  cwd "$rootdir" fun package-backend-alleycat
  fun build-image "$opt_C"
}

cmd-update-sources () {
  fun update-sources
}

cmd-run () {
  local opt_d=
  local opt_pre=
  while getopts hd-: arg; do
      case $arg in
          h) warn "$USAGE"; exit 0 ;;
          d) opt_d=yes ;;
          -)  OPTARG_KEY="${OPTARG%=*}"
              OPTARG_VALUE="${OPTARG#*=}"
              case $OPTARG_KEY in
                  help)  warn "$USAGE"; exit 0 ;;
                  pre) opt_pre="$OPTARG_VALUE" ;;
                  '')    break ;;
                  *)     error "Illegal option --$OPTARG" ;;
                  esac ;;
          *) error "$USAGE" ;;
      esac
  done
  shift $((OPTIND-1))

  mci
  mcb docker run
  mcb   -it --rm
  mcb   --name "$docker_container_name"
  if [ "$opt_d" = yes ]; then
  mcb   -d
  fi
  mcb   -p "$host_http_alleycat_port":"$container_nginx_alleycat_port"
  mcb   -p "$host_http_sinatra_port":"$container_nginx_sinatra_port"
  mcb   -p "$host_upsycle_port":"$container_upsycle_port"
  mcb   -p "$host_upsycle_control_port":"$container_upsycle_control_port"
  mcb   -v "$dmc_data_dir":/root/.local/share/dmc
  mcb   $opt_pre
  mcb   --
  mcb   "$docker_image_name"
  mcb   "$@"
  mcg
}

if [ "$#" -lt 1 ]; then error "$USAGE"; fi

if [ "$1" = init ]; then
  if [ ! "$#" = 1 ]; then error "$USAGE"; fi
  fun cmd-init
elif [ "$1" = update-sources ]; then
  if [ ! "$#" = 1 ]; then error "$USAGE"; fi
  fun cmd-update-sources
elif [ "$1" = build ]; then
  if [ ! "$#" = 1 -a ! "$#" = 2 ]; then error "$USAGE"; fi
  shift
  fun cmd-build "$@"
elif [ "$1" = run ]; then
  shift
  fun cmd-run "$@"
else
  error "$USAGE"
fi
