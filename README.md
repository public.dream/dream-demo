## Build the Docker image

Note that this step is only for development: you can use the pre-built image
`alleycatcc/dream-demo:latest`

### Initialize:

    git submodule update --init --recursive
    cp vars-local.sh-example vars-local.sh
    # --- alter vars-local.sh as necessary.
    bin/run init

### (Re)build:

    # --- rerun these two steps to keep the git repos of the sources updated if there are new commits.
    bin/run update-sources
    bin/run build

## Run the demo

- Prebuilt image:

        # --- use -d to detach the TTY
        DOCKER_IMAGE_NAME=alleycatcc/dream-demo:latest bin/run run [-d]

- If you built it yourself:

        # --- use -d to detach the TTY
        bin/run run [-d]

- Browse to http://localhost:$host_http_sinatra_port (http://localhost:33334 by default).

- To use DMC or RUN interactively:

        source demo-source.sh
        dmc-cmd --help
        run-cmd h
    
- To call the DMC binary directly:

        ( . vars-local.sh && docker exec -it "$docker_container_name" /bin/bash )
        # --- inside Docker container:
        # dmc --help

